//Write a program to find the Frequency of a given character in a given string.
#include<stdio.h>
#include<conio.h>
#define MAXsize 100
int frequency(char a[MAXsize],char b,int c);
int main()
{
    char sentence[MAXsize];
    char ch;
    int count=0,i;
    printf("Enter your sentence to find the frequency--->");
    gets(sentence);
    printf("Which character's frequency do you want?");
    scanf("%c",&ch);

    for(i=0;sentence[i]!='\0';i++)
    {
        if(ch==sentence[i])
            count++;
    }
    printf("Frequency of character '%c' is %d",ch,count);
    return 0;
}


