//Write a C program to reverse a sentence entered by user.
#include<stdio.h>
#include <string.h>
#define MAXsize 100
int reverse(char sentence[],int a);
int main()
{
    char sentence[MAXsize];
    int size,i;
    printf("Enter your sentence to reverse:\t");
    gets(sentence);
    printf("***The reversed sentence***\n");
    size=strlen(sentence)-1;
    reverse(sentence,size);
    printf("\n");
    return 0;
}
int reverse(char sentence[],int a)
{
    if(a>=0)
    {
        printf("%c",sentence[a]);
        reverse(sentence,a-1);
    }
}


